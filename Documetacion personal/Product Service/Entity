--------------------------------------PRODUCT-SERVICE primer micro servicio del proyecto de Digital Lab--------------------------------------

-Consiste en un CRUD(Create, READ, Update, DELETE) de una base de datos de los productos de una tienda.

-Paquete Entity:
    
    1) Category: Clase que sera persistida en la base de datos representando la categoria de un producto.

            -Notaciones de la clase:
                * @Entity Especificamos que la clase es una entidad.

                * @Table Especifica la tabla primaria para la entidad. Se pueden especificar tablas adicionales 
                mediante la anotación @SecondaryTable o @SecondaryTables.
                Si no se especifica una anotación de tabla para una clase de entidad, se aplican los valores predeterminados
                en este caso el nombre no seria 'categories' seria 'Category'.

                *Las siguientes notaciones pertenecen a lombok
                    * @Data genera getters, setters, equals and hashcode, toString y un constructor que requiere argumentos.
                    
                    * @AllArgsConstructor genera un constructor con todos los argumentos o atributos de la clase.
                    
                    * @NoArgsConstructor genera un constructor por defecto.
                    
                    * @Builder Si se anota una clase, se genera un constructor privado con todos los campos como argumentos como si 
                    @AllArgsConstructor (access = AccessLevel.PRIVATE) esté presente en la clase, y es como si este constructor hubiera 
                    sido anotado con @Builder en su lugar.

                    Este constructor solo se genera si no ha escrito ningún constructor y tampoco ha agregado ninguna anotación explícita de 
                    @XArgsConstructor. En esos casos, lombok asumirá que está presente un constructor de todos los argumentos y
                    generará el código que lo usa; Esto significa que obtendría un error de compilación si este constructor no está presente.

                    El efecto de @Builder es que se genera una clase interna llamada T Builder, con un constructor privado. Las instancias de 
                    T Builder se realizan con el método denominado builder(), que también se genera para usted en la clase en
                    sí (no en la clase interna builder).

                    La clase T Builder contiene 1 método para cada parámetro del constructor / método anotado, para inicializarlos. 
                    El constructor también tiene un método build () que devuelve una instancia completa del tipo original, creado al
                    pasar todos los parámetros establecidos a través de los otros métodos del constructor al constructor o método anotado con 
                    @Builder.
                    
                    El tipo de retorno de este método será el mismo que la clase relevante, a menos que se haya anotado un método, en cuyo caso
                    será igual al tipo de retorno de ese método.

                    Todo esto es documentacion oficial, ver el test ProductRepositoryMockTest para ver la implementacion de esos metodos.

!!!!!!!!!!!!!!!!!!!!Como esto es de lombok y sts no puede usarlo sin especificar la ruta de sts en el jar de lombok buscar el jar y
                        ejecutarlo con el comando java -jar nombreyversion.jar esta en la carpeta .m2 del directorio home de tu usuario
                        en el sistema operativo que uses, donde se guardan las dependencias del proyecto!!!!!

            -Notaciones de los atributos:
                * @Id para especificar el atributo que actuara como llave primaria de la entidad en la base de datos.

                * @GeneratedValue especifica que el el valor del atributo sera generado automaticamente segun la estrategia que se use como 
                esto es una entidad se uso esa estrategia.

    2) Product: Clase que sera persistida en la base de datos representando un producto y posee una relacion many to one con Category.

            -Notaciones de la clase: Se usaron las mismas notaciones que en la calse anterior.

            -Notaciones de los atributos:
                Las siguientes notaciones son usadas para validar los campos y pertenecen a javax.validation.constraints.* que viene en la 
                dependencia spring-boot-starter-validation desde la version 2.3.0 de spring boot ya no incluye esta dependencia, 
                tienes que incluirla en el pom.xml.

                * @Size el atributo o campo anotado con esto debera tener un tamaño comprendido entre los valores fijados por min y max o se 
                mostrara un mensaje de formato invalido por lo que se tomara como null y el objeto no sera persistido.
                -> Tipos soportados: secuencias o cadenas de caracteres, Colleciones, Mapas y Arrays

                * @NotEmpty el atributo o campo no puede ser null o vacio. Soporta los mismos tipos que la notacion anterior.

                * @Positive el atributo numerico debe ser positivo. Soporta los tipos: BigDecimal, BigInteger, byte, short, int, long, float, double
                y sus respectivos wrappers.

                * @NotNull el atributo o elemento anotado con esto no debe ser null. Soporta cualquier tipo.

                Ahora las notaciones de javax.persistence aunque fueron pocas las usadas en esta clase, ademas que las notaciones del campo Id
                son las mismas que en la clase anterior 'Entity'.

                * @Column especifica que el campo o atributo sera persistido como una columna, tiene varios metodos como unique, nullable, 
                name, etc. Y si no se especifica nada de esto la tabla tendra valores por defecto.

                * @Temporal notacion para mapear las fechas de tipo util.Date a sql.Date las fechas util.Date solo guardan un registro de tipo
                long que representa los milisegundos desde una fecha especifica hasta la fecha que tu defines las sql.Date son el registro en
                si de la fecha para poder trabajarlas en las bases de datos, solo tiene un metodo TemporalType donde DATE solo registra la fecha, 
                TIME registra solo el tiempo, TIMESTAMP los dos.

                * @ManyToOne especifica que esta entidad tendra una relacion de muchos a uno con otra. esta notacion tiene 4 metodos pero aqui 
                solo se utilizo el metodo de busca de tipo peresozo para que no cargue todos los datos de la otra entidad a menos que sea especificado.

                *@JoinColumn especifica cual sera la columna de union para las entidades relacionadas.

                Ahora una notacion que necesita investigarse mas:
                * @JsonIgnoreProperties no me queda claro que soluciona o si tiene que ver solo con el @ManyToOne y el fetch lazy ya que recive
                el error que muestra el postman o si se puede evitar su uso de alguna manera.
                Recordar que este proyecto fue fiel en lo que se pudo al visto de Digital Lab por lo que la relacion deberia ser perteneciente
                a Category no a Product puede que sea ese el error uno que no se explicar puede que sea de logica ya que usar lazy solo por un 
                registro no me parece tan perjudicial. 
                Documentacion oficial: Anotación que se puede utilizar para suprimir la serialización de propiedades (durante la serialización) 
                o ignorar el procesamiento de las propiedades de JSON leídas (durante la deserialización).