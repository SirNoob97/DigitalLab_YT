package com.sirnoob.customerservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name = "customers")
@Data
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "El numero de documento es obligatorio")
	@Size(min = 8, max = 8, message = "El numero de documento debe ser de 8 digitos")
	@Column(name = "number_id", unique = true, nullable = false)
	private String numberId;

	@NotEmpty(message = "El Nombre es obligatorio")
	@Column(nullable = false)
	private String name;

	@NotEmpty(message = "El Apellido es obligatorio")
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@NotEmpty(message = "El email es obligatorio")
	@Email(message = "Formato de Email invalido")
	@Column(unique = true, nullable = false)
	private String email;

	@Column(name = "photo_url")
	private String photoUrl;

	@NotNull(message = "La region es obligatoria")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Region region;

	private String state;
}
