package com.sirnoob.customerservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sirnoob.customerservice.entity.Customer;
import com.sirnoob.customerservice.entity.Region;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	public Customer findByNumberId(String numberId);

	public List<Customer> findByLastName(String lastName);

	public List<Customer> findByRegion(Region region);

}
