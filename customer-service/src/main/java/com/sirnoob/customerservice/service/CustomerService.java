package com.sirnoob.customerservice.service;

import java.util.List;

import com.sirnoob.customerservice.entity.Customer;
import com.sirnoob.customerservice.entity.Region;

public interface CustomerService {

	public List<Customer> findAllCustomer();

	public List<Customer> findCustomerByRegion(Region region);

	public Customer createCustomer(Customer customer);

	public Customer updateCustomer(Customer customer);

	public Customer deleteCustomer(Customer customer);

	public Customer getCustomer(Long id);
}
