package com.sirnoob.productservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sirnoob.productservice.entity.Category;
import com.sirnoob.productservice.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	public List<Product> findByCategory(Category category);
	public Product findByNumberId(String numberId);
}
