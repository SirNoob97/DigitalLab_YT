package com.sirnoob.productservice.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sirnoob.productservice.entity.Category;
import com.sirnoob.productservice.entity.Product;
import com.sirnoob.productservice.repository.ProductRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> listAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public Product getProduct(Long id) {
		return productRepository.findById(id).orElse(null);
	}

	@Override
	public Product createProduct(Product product) {
		Product productDB = productRepository.findByNumberId(product.getNumberId());
		if (productDB != null)
			return productDB;

		product.setStatus("CREATED");
		product.setCreateAt(new Date());

		productDB = productRepository.save(product);
		return productDB;
	}

	@Override
	public Product updateProduct(Product product) {
		Product productDB = getProduct(product.getId());

		if (productDB == null)
			return null;

		productDB.setName(product.getName());
		productDB.setDescription(product.getDescription());
		productDB.setCategory(product.getCategory());
		productDB.setPrice(product.getPrice());

		return productRepository.save(productDB);
	}

	@Override
	public Product deleteProduct(Long id) {
		Product productDB = getProduct(id);

		if (productDB == null)
			return null;

		productDB.setStatus("DELETED");

		return productRepository.save(productDB);
	}
//	metodo para eliminar los registros de la base de datos el anterior solo setea el estado a DELETE
//	@Override
//	public void deleteProduct(Long id) {
//		Product productDB = getProduct(id);		
//		productRepository.delete(productDB);
//	}

	@Override
	public List<Product> findByCategory(Category category) {
		return productRepository.findByCategory(category);
	}

	@Override
	public Product updateStock(Long id, Double quantity) {
		Product productDB = getProduct(id);

		if (productDB == null)
			return null;

		double stock = productDB.getStock() + quantity;

		productDB.setStock(stock);
		return productRepository.save(productDB);
	}

}
