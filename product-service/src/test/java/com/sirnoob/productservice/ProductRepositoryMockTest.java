package com.sirnoob.productservice;

import java.util.Date;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.sirnoob.productservice.entity.Category;
import com.sirnoob.productservice.entity.Product;
import com.sirnoob.productservice.repository.ProductRepository;

@DataJpaTest
public class ProductRepositoryMockTest {	
	
	private final ProductRepository productRepository;

	@Autowired
	public ProductRepositoryMockTest(ProductRepository productRepository) {	
		this.productRepository = productRepository;
	}

	@Test
	public void whenFindByCategory_thenReturnListProduct() {
		Product product = Product.builder()
				.name("Computer")
				.category(Category.builder().id(1L).build())
				.description("")
				.stock(Double.parseDouble("10"))
				.price(Double.parseDouble("100"))
				.status("Created")
				.createAt(new Date())
				.build();

		productRepository.save(product);
		List<Product>founds = productRepository.findByCategory(product.getCategory());
		Assertions.assertThat(founds.size()).isEqualTo(3);
	}
}
