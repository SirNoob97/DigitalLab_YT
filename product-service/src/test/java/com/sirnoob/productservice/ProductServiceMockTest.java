package com.sirnoob.productservice;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.sirnoob.productservice.entity.Category;
import com.sirnoob.productservice.entity.Product;
import com.sirnoob.productservice.repository.ProductRepository;
import com.sirnoob.productservice.service.ProductService;
import com.sirnoob.productservice.service.ProductServiceImpl;

//Anotación para una clase de prueba que ejecuta pruebas basadas en Spring Boot.
@SpringBootTest
public class ProductServiceMockTest {

	/*
	 * Marcar un field como un mock = simulacro en este contexto se traduce asi.
	 * 
	 * en la documentacion oficial especifican estas cualidades:
	 * Permite la creación de simulacros de taquigrafía. 
	 * Minimiza el código repetitivo de creación simulada. 
	 * Hace que la clase de prueba sea más legible.
	 * Hace que el error de verificación sea más fácil de leer porque el nombre del
	 * campo se usa para identificar el simulacro.
	 */
	@Mock
	private ProductRepository productRepository;
	
	private ProductService productService;
	
	
	/*
	 * Documentacion oficial:
	 * @BeforeEach se usa para indicar que el método anotado se debe ejecutar antes
	 * de cada método @Test, @RepeatedTest, @ParameterizedTest, @TestFactory
	 * y @TestTemplate en la clase de prueba actual.
	 */
	@BeforeEach
	public void setup() {
		
		/*
		 * esta linea inicializa los fields anotados con notaciones de Mockito en
		 * la documentacion oficial recomienda ver la interface MockitoSessions que no solo los
		 * inicializa sino que tambien agrega validaciones adicionales
		 */		
		MockitoAnnotations.initMocks(this);
		 
		productService = new ProductServiceImpl();
		
		Product computer = Product.builder()
				.id(1L)
				.name("Computer")
				.category(Category.builder().id(1L).build())
				.price(Double.parseDouble("12.5"))
				.stock(Double.parseDouble("5"))
				.build();
		
		/*
		 * el primer simulacro retornara por si se hacran mas operaciones un producto cuando encuentre uno con 
		 * el id especificado el siguiente lo guardara en la DB y lo retornara para posibles operaciones futuras
		 * pero esto es un test asi que se hara un rollback a la DB una vez termine la ejecucion de este metodo
		 */		
		Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(computer));
		Mockito.when(productRepository.save(computer)).thenReturn(computer);
	}
	
	@Test
	public void whenValidateGetId_thenReturnProduct() {
		Product found = productService.getProduct(1L);
		Assertions.assertThat(found.getName()).isEqualTo("Computer");
	}
	
	@Test
	public void whenValidateUpdateStock_thenReturnNewStock() {
		Product newStock = productService.updateStock(1L, Double.parseDouble("8"));
		Assertions.assertThat(newStock.getStock()).isEqualTo(13);		
	}
}
