package com.sirnoob.shoppingservice.client;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.sirnoob.shoppingservice.model.Product;

@Component
public class ProductHystrixFallbackFactory implements ProductClient {

	@Override
	public ResponseEntity<Product> getProduct(Long id) {

		Product product = Product.builder()
				.name("none")
				.description("none")
				.price(0.0)
				.stock(0.0).build();

		return ResponseEntity.ok(product);
	}

	@Override
	public ResponseEntity<Product> updateStockProduct(Long id, Double quantity) {
		
		Product product = Product.builder()
				.name("none")
				.description("none")
				.price(0.0)
				.stock(0.0).build();

		return ResponseEntity.ok(product);
	}

}
