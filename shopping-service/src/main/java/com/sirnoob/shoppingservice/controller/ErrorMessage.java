package com.sirnoob.shoppingservice.controller;

import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorMessage {

	private String code;
	private List<Map<String, String>>messages;
}
