package com.sirnoob.shoppingservice.model;

import lombok.Data;

@Data
public class Category {

	private Long id;
	private String name;
}
