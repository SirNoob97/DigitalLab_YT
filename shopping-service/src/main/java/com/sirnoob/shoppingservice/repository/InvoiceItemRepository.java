package com.sirnoob.shoppingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sirnoob.shoppingservice.entity.InvoiceItem;

public interface InvoiceItemRepository extends JpaRepository<InvoiceItem, Long>{

}
