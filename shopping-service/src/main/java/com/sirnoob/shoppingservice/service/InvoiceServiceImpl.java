package com.sirnoob.shoppingservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sirnoob.shoppingservice.client.CustomerClient;
import com.sirnoob.shoppingservice.client.ProductClient;
import com.sirnoob.shoppingservice.entity.Invoice;
import com.sirnoob.shoppingservice.entity.InvoiceItem;
import com.sirnoob.shoppingservice.model.Customer;
import com.sirnoob.shoppingservice.model.Product;
import com.sirnoob.shoppingservice.repository.InvoiceRepository;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private CustomerClient customerClient;

	@Autowired
	private ProductClient productClient;

	@Override
	public List<Invoice> findInvoiceAll() {
		return invoiceRepository.findAll();
	}

	@Override
	public Invoice createInvoice(Invoice invoice) {
		Invoice invoiceDB = invoiceRepository.findByNumberInvoice(invoice.getNumberInvoice());
		if (invoiceDB != null)
			return invoiceDB;

		invoice.setStatus("CREATED");
		invoiceDB = invoiceRepository.save(invoice);

		invoiceDB.getItems()
				.forEach(item -> productClient.updateStockProduct(item.getProductId(), item.getQuantity() * -1));

		return invoiceDB;
	}

	@Override
	public Invoice updateInvoice(Invoice invoice) {
		Invoice invoiceDB = getInvoice(invoice.getId());
		if (invoiceDB == null)
			return null;

		invoiceDB.setCustomerId(invoice.getCustomerId());
		invoiceDB.setDescription(invoice.getDescription());
		invoiceDB.setNumberInvoice(invoice.getNumberInvoice());
		invoiceDB.getItems().clear();
		invoiceDB.setItems(invoice.getItems());

		return invoiceRepository.save(invoiceDB);
	}

	@Override
	public Invoice deleteInvoice(Invoice invoice) {
		Invoice invoiceDB = getInvoice(invoice.getId());
		if (invoiceDB == null)
			return null;

		invoiceDB.setStatus("DELETED");

		return invoiceRepository.save(invoiceDB);
	}

	@Override
	public Invoice getInvoice(Long id) {
		Invoice invoice = invoiceRepository.findById(id).orElse(null);

		if (invoice != null) {
			Customer customer = customerClient.getCustomer(invoice.getCustomerId()).getBody();
			invoice.setCustomer(customer);

			List<InvoiceItem> listItems = invoice.getItems().stream().map(item -> {
				
				Product product = productClient.getProduct(item.getProductId()).getBody();
				item.setProduct(product);
				
				return item;
			}).collect(Collectors.toList());

			invoice.setItems(listItems);
		}

		return invoice;
	}

}
